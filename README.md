# A&B Toys Inventory Management System

This is a simple inventory management system for A&B Toys, developed in C++ using Object-Oriented Programming (OOP) principles. The system allows you to manage toy records, including adding, deleting, updating, and displaying toy information.

Access Wiki [Here](https://gitlab.une.edu.au/cosc220_software_development_group_assignment/csc220_sdga_inventory_system/-/wikis/A&B-Toys-Inventory-Management-System)
## Features

- Add a new toy to the inventory.
- Delete a toy from the inventory.
- Update toy information.
- Display a list of all toys in the inventory.

## Getting Started

### Prerequisites

- C++ compiler (e.g., g++)
- SQLite library (for database functionality)

### Installation

1. Clone the repository:

   ```bash
   git clone <repository_url>
   cd <repository_directory>

2. Build and run the program

   ```bash
   gcc sqlite3.c -c
   g++ inventory_management_system.cpp sqlite3.o -I.
   ./a.exe

### Usage
Follow the on-screen menu to perform inventory management operations.
You can add, delete, update, and display toy records as needed.
Contributing
Contributions are welcome! If you'd like to contribute to this project, please follow these steps:

### Fork the repository
Create a new branch for your feature or bug fix.
Make your changes and commit them.
Push your changes to your fork.
Create a pull request, describing your changes.

### Acknowledgments
Special thanks to 'nbagga' and 'hkaur28' for their contributions to this project.
