# Contributing to A&B Toys Inventory Management System

We welcome contributions to the A&B Toys Inventory Management System. By contributing, you can help improve the project and make it more robust. Before you start contributing, please take a moment to read through this guide.

## Contributors

We would like to acknowledge and thank the following contributors for their valuable contributions to this project:

- [hkaur28](https://github.com/hkaur28)
- [nbagga](https://github.com/nbagga)

## How to Contribute

1. Fork the repository by clicking the "Fork" button on the top right corner of the repository's GitHub page.

2. Clone your forked repository to your local machine:

   ```bash
   git clone <your_forked_repository_url>
   cd <your_forked_repository_directory>
