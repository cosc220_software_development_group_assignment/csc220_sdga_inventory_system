#include <iostream>
#include <string>
#include <vector>
#include "sqlite3.h"

using namespace std;

class Toy {
private:
    string name;
    string material;
    string description;
    double manufacturingPrice;
    int quantityInStock;

public:
    Toy(string n, string m, string desc, double price, int quantity)
        : name(n), material(m), description(desc), manufacturingPrice(price), quantityInStock(quantity) {}

    // Getter methods
    string getName() const { return name; }
    string getMaterial() const { return material; }
    string getDescription() const { return description; }
    double getManufacturingPrice() const { return manufacturingPrice; }
    int getQuantityInStock() const { return quantityInStock; }

    // Setter methods
    void setName(string n) { name = n; }
    void setMaterial(string m) { material = m; }
    void setDescription(string desc) { description = desc; }
    void setManufacturingPrice(double price) { manufacturingPrice = price; }
    void setQuantityInStock(int quantity) { quantityInStock = quantity; }

    // Display toy details
    void display() const {
        cout << "Toy Name: " << name << endl;
        cout << "Material: " << material << endl;
        cout << "Description: " << description << endl;
        cout << "Manufacturing Price: $" << manufacturingPrice << endl;
        cout << "Quantity in Stock: " << quantityInStock << endl;
    }
};

class Inventory {
private:
    sqlite3* db; // SQLite database handle
    vector<Toy> toys;

public:
    Inventory() {
        int rc = sqlite3_open("inventory.db", &db);
        if (rc) {
            cerr << "Can't open database: " << sqlite3_errmsg(db) << endl;
            sqlite3_close(db);
            exit(1);
        }
        // Create the "toys" table if it doesn't exist
        const char* createTableSQL =
            "CREATE TABLE IF NOT EXISTS toys ("
            "id INTEGER PRIMARY KEY AUTOINCREMENT,"
            "name TEXT NOT NULL,"
            "material TEXT NOT NULL,"
            "description TEXT,"
            "manufacturingPrice REAL NOT NULL,"
            "quantityInStock INTEGER NOT NULL"
            ");";

        rc = sqlite3_exec(db, createTableSQL, 0, 0, 0);
        if (rc) {
            cerr << "SQL error: " << sqlite3_errmsg(db) << endl;
            sqlite3_close(db);
            exit(1);
        }
    }

    ~Inventory() {
        sqlite3_close(db);
    }
    // hkaur28 methods added
    // Add a new toy to the inventory and the database
    void addToy(const Toy& toy) {
        toys.push_back(toy);

        const char* insertSQL = "INSERT INTO toys (name, material, description, manufacturingPrice, quantityInStock) "
                                "VALUES (?, ?, ?, ?, ?);";

        sqlite3_stmt* stmt;
        int rc = sqlite3_prepare_v2(db, insertSQL, -1, &stmt, 0);
        if (rc != SQLITE_OK) {
            cerr << "SQL error: " << sqlite3_errmsg(db) << endl;
            return;
        }

        sqlite3_bind_text(stmt, 1, toy.getName().c_str(), -1, SQLITE_STATIC);
        sqlite3_bind_text(stmt, 2, toy.getMaterial().c_str(), -1, SQLITE_STATIC);
        sqlite3_bind_text(stmt, 3, toy.getDescription().c_str(), -1, SQLITE_STATIC);
        sqlite3_bind_double(stmt, 4, toy.getManufacturingPrice());
        sqlite3_bind_int(stmt, 5, toy.getQuantityInStock());

        rc = sqlite3_step(stmt);
        if (rc != SQLITE_DONE) {
            cerr << "SQL error: " << sqlite3_errmsg(db) << endl;
        }

        sqlite3_finalize(stmt);
    }

    // Delete a toy from the inventory and the database by its name
    void deleteToy(const string& name) {
        auto it = toys.begin();
        while (it != toys.end()) {
            if (it->getName() == name) {
                it = toys.erase(it);
            } else {
                ++it;
            }
        }

        const char* deleteSQL = "DELETE FROM toys WHERE name = ?;";

        sqlite3_stmt* stmt;
        int rc = sqlite3_prepare_v2(db, deleteSQL, -1, &stmt, 0);
        if (rc != SQLITE_OK) {
            cerr << "SQL error: " << sqlite3_errmsg(db) << endl;
            return;
        }

        sqlite3_bind_text(stmt, 1, name.c_str(), -1, SQLITE_STATIC);

        rc = sqlite3_step(stmt);
        if (rc != SQLITE_DONE) {
            cerr << "SQL error: " << sqlite3_errmsg(db) << endl;
        }

        sqlite3_finalize(stmt);
    }

    // Update a toy's information in the inventory and the database by its name
    void updateToy(const string& name, const Toy& newToy) {
        auto it = toys.begin();
        while (it != toys.end()) {
            if (it->getName() == name) {
                *it = newToy;
                break;
            } else {
                ++it;
            }
        }

        const char* updateSQL = "UPDATE toys SET name = ?, material = ?, description = ?, "
                                "manufacturingPrice = ?, quantityInStock = ? WHERE name = ?;";

        sqlite3_stmt* stmt;
        int rc = sqlite3_prepare_v2(db, updateSQL, -1, &stmt, 0);
        if (rc != SQLITE_OK) {
            cerr << "SQL error: " << sqlite3_errmsg(db) << endl;
            return;
        }

        sqlite3_bind_text(stmt, 1, newToy.getName().c_str(), -1, SQLITE_STATIC);
        sqlite3_bind_text(stmt, 2, newToy.getMaterial().c_str(), -1, SQLITE_STATIC);
        sqlite3_bind_text(stmt, 3, newToy.getDescription().c_str(), -1, SQLITE_STATIC);
        sqlite3_bind_double(stmt, 4, newToy.getManufacturingPrice());
        sqlite3_bind_int(stmt, 5, newToy.getQuantityInStock());
        sqlite3_bind_text(stmt, 6, name.c_str(), -1, SQLITE_STATIC);

        rc = sqlite3_step(stmt);
        if (rc != SQLITE_DONE) {
            cerr << "SQL error: " << sqlite3_errmsg(db) << endl;
        }

        sqlite3_finalize(stmt);
    }

    // Display all toys in the inventory
    void displayInventory() const {
        cout << "=== Inventory ===" << endl;
        for (const Toy& toy : toys) {
            toy.display();
            cout << "----------------" << endl;
        }
    }
    // nbagga UI Menu
    // Display a menu to interact with the inventory
    void displayMenu() {
        while (true) {
            cout << "=== Inventory Management ===" << endl;
            cout << "1. Add Toy" << endl;
            cout << "2. Delete Toy" << endl;
            cout << "3. Update Toy" << endl;
            cout << "4. Display Inventory" << endl;
            cout << "5. Exit" << endl;
            cout << "Enter your choice: ";

            int choice;
            cin >> choice;

            switch (choice) {
                case 1: {
                    Toy newToy = inputToyDetails();
                    addToy(newToy);
                    break;
                }
                case 2: {
                    string name;
                    cout << "Enter the name of the toy to delete: ";
                    cin.ignore();
                    getline(cin, name);
                    deleteToy(name);
                    break;
                }
                case 3: {
                    string name;
                    cout << "Enter the name of the toy to update: ";
                    cin.ignore();
                    getline(cin, name);
                    Toy updatedToy = inputToyDetails();
                    updateToy(name, updatedToy);
                    break;
                }
                case 4:
                    displayInventory();
                    break;
                case 5:
                    return;
                default:
                    cout << "Invalid choice. Please try again." << endl;
            }
        }
    }

    Toy inputToyDetails() {
        string name, material, description;
        double manufacturingPrice;
        int quantityInStock;

        cout << "Enter Toy Name: ";
        cin.ignore();
        getline(cin, name);
        cout << "Enter Material: ";
        getline(cin, material);
        cout << "Enter Description: ";
        getline(cin, description);
        cout << "Enter Manufacturing Price: $";
        cin >> manufacturingPrice;
        cout << "Enter Quantity in Stock: ";
        cin >> quantityInStock;

        Toy newToy(name, material, description, manufacturingPrice, quantityInStock);
        return newToy;
    }
};

int main() {
    Inventory inventory;
    inventory.displayMenu();

    return 0;
}